##  Daily Summary 

**Time: 2023/7/11**

**Author: 赖世龙**

---

**O (Objective):**  Today, the teachers explained Tasking very carefully. Through multiple training, I learned how to use Tasking and Context map to solve business problems and split a big business into many small steps. 

**R (Reflective):**   Meaningful 

**I (Interpretive):**  I learned a lot of very practical ideas and skills today, which I think can be applied to my future work and life. Today is a full and meaningful day.

**D (Decisional):**  I will fully use the Tasking to the company's business and try to divide the big business into small steps so as to better execute.

